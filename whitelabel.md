# Whitelabel site

Our whitelabel site is a copy of camplace.com, branded to look like part of your site. Members can use their accounts on your site for login in the whitelabel, similarly how facebook login works. Escorts can only login/register this way.

## Communication

There are 2 ways of communication between our system and your system:
- server-to-server json api calls, your server -> our server
    - docs: [escort-api.md](/escort-api-v2.md)
    - authentication: we will send you a shared secret that you need to send us in a `X-Server-Secret` header
- server-to-server json api callbacks, our server -> your server
    - TBD, we will add this if it is needed for something

## Base URLs
- escort api:
    - dev: `https://escort.dev-dliver.prmrgt.com`
    - prod: `https://escort.dliver.prmrgt.com`

## Variables
- `remote_id`/`profile_remote_id`: the unique id used in your system for the escort or member (depending on the route)
- `site_group_code`: a fixed string, we will send it to you
- `site_id`/`signup_site_id`: a fixed string, we will send it to you

## User flows

### Login when user is on your site
1. Member logs in on your site
2. Member clicks a button to go to the cams page
3. Your backend calls our escort api Member Login endpoint with information about the member (**only send data that you wish to update on every login**), the escort api returns a one time token to your backend
    - if the member does NOT exist yet, the api will return a 404 error with code `ERR_MEMBER_NOT_FOUND`
        - in this case, you should call our escort api Member Create endpoint, with all the data you can supply about the member
4. You return this one time token to the user's browser, and open the whitelabel site, with the token passed as a query parameter (call it: 'one-time-token')
5. The whitelabel site logs in the user using the provided token

### Login when user is on our site
1. User clicks login button, the login modal appears
2. On the login modal, there is a button: `Login with using <your site>`, the user clicks it
3. We redirect to your site (or open it in a popup), taking the user to your login page
4. Member logs in on your site, flow continues from step 3. of `Login flow when user is on your site`

### Login for escorts
1. Escort logs in on your site
2. Escort clicks a button to go to the broadcast page
3. Your backend calls our escort api Escort Login endpoint with information about the escort (**only send data that you wish to update on every login**), the escort api returns a one time token to your backend
    - if the escort does NOT exist yet, the api will return a 404 error with code `ERR_ESCORT_NOT_FOUND`
        - in this case, you should call our escort api Escort Create endpoint, with all the data you can supply about the escort
4. You return this one time token to the escort's browser, and open the whitelabel site, with the token passed as a query parameter (call it: 'one-time-token')
5. The whitelabel site logs in the escort using the provided token

### Login for agencies
1. Agency logs in on your site
2. Agency clicks on a button to go to our cam site
3. Your backand calls our escport api Agency Login endpoint with information about the agency (**only send data that you wish to update on every login**), the escort api returns a one time token to your backend
    - **NOTE**: sending the list of escorts belonging to the agency on a login request will override all escorts, disabling any previously added escorts on our site that are not in the current list 
    - if the agency does NOT exist yet, the api will return a 404 error with code `ERR_AGENCY_NOT_FOUND`
        - in this case, you should call our escort api Agency Create endpoint, with all the data you can supply about the agency, including the escorts belonging to it
4. You return this one time token to the agency's browser, and open the whitelabel site, with the token passed as a query parameter (call it: 'one-time-token')
    - we recommend that you send the agency to `/studios/models` page
5. The whitelabel site logs in the agency using the provided token
6. Agencies can see all their escorts under `/studios/models` page, from here the registration for these escorts can be finished (see: [Finish escort registration](#finish-escort-registration))

### Finish escort registration
Escorts belonging to agencies might not be able to login on the escort site (because their agency manages all their data).
However, in order to be able to stream on our site, they must be able to log in on the whitelabel site.

1. Agency is already registered on our site (see: [Login for agencies](#login-for-agencies))
2. Agency must log in on the whitelabel site, and navigate to `/studios/models`
3. Here the agency will see all it's models (escorts). For all models with unfinished registration they have 2 options for finishing it:
    a.) The agency can click on the "Finish registration" button, and fill in the required data in the modal that comes up
    b.) The agency can click on "Copy finish registration to clipboard" button. Using this link, the escort can finish her own registration.

### Subscription activation/deactivation
On escort sites, escorts need to pay a subscription fee to advertise on the site. Broadcasting on our whitelabel site
can be disabled in case the escort does NOT have a valid subscription.

1. On escort api Escort/Agency Register/Login/Update routes, there is a `subscription_active` field.
2. Setting it to `false` will show a warning to the model (escort) on the whitelabel site, similar to this:
    - "In order to have full access to the cam module, please ensure that your profile has an active package on
     <your domain>. _Go to your Escortforum account_ to purchase a package or check the status of your profile."

### Agency login flow
1. agency calls escort service agency login route
    - models are synchronized
2. same as registration flow from step 2

### Booking
1. escort configures booking options on your site
   - use escort update route to update booking related info (see video_booking_* and phone_number fields)
2. member clicks on booking create button on your site
   - `https://<sitename>.escortcams.dev/escort/<profile_remote_id>/channel?booking-purchase-modal=1&one-time-token=...`
      - `profile_remote_id` is the id you send in during escort register/login/update requests 
      - `booking-purchase-modal=1` will show the modal for buying a booking

## Docs

### Escort api

[escort-api.md](/escort-api-v2.md) - written in [api blueprint](https://apiblueprint.org/)
