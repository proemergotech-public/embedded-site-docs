# Messages between your site and the iframe

## Message structure

`{"action": "<action>", data: {<action specific data>}}`

## Sending a message to our iframe

`myIframe.contentWindow.postMessage({"action": "<action>", data: {<action specific data>}}, '*');`

## Receiving messages from our iframe

```js
window.onmessage = function(e){
    const message = e.data
    console.log(message.action, message.data)
};

// OR

window.addEventListener("message", function(e){
   const message = e.data
   console.log(message.action, message.data)
}, false);
```

## Messages sent from your site to ours

### Login/logout
As sessions are handled on your site, you can use postMessage to login/logout in our site. For login you can use the
token returned from our escort api login methods.

Example:
    - `{"action": "login", data: {"token": "<token>"}}`
    - `{"action": "login", data: {"token": "<token>"}}`

### Navigate
Instead of reloading the iframe content on navigation (eg: when the user clicks on the browser back button), you can
send a relative url to our site.

Example:
    - `{"action": "navigate", data: {"url": "/channel/test-model-xxx1-test-model-xxx1"}}`

## Messages sent from our site to yours

### Opening login, registration and purchase modals
On our site, certain functions require login/registration. In this case we signal your site to open a login or 
registration modal using postMessage. If the member would like to use a function that requires more tokens than he 
already has, we signal the need to open a purchase modal similarly.

Example:
- `{"action": "open_login", "data": {}}`
- `{"action": "open_reg", "data": {}}`
- `{"action": "open_purchase", "data": {}}`

### Balance display
As we will have no header on our site, you should display the balance of the members. We will send the current balance
using postMessage.

Note that we do NOT round the balance, meaning you will have to round it to the desired digits when displaying (we 
recommend 2 digits).

Example:
- `{"action": "balance", "data": {"balance": 123.6564}}`

### URL handling
If the user navigates within our site (eg: opens a channel), we will send a postMessage. You should update your url to
reflect this, eg:
- if our site is embedded under: `<your domain>/cams`
- and we send an url change with `"url" = "/channel/test-channel"`
- you should change your url to: `<your domain>/cams/channel/test-channel`

Example:
- `{"action": "url_change", "data": {"url": "/channel/test-channel"}}`

    
### IFrame height
In order to support using your footer on your site, we send the current height of the iframe content. You should set the
height of the iframe to this value.

**There is a special case when we send "full_page" as height value. In this case our content will fill 100% of the 
window height, so you should set the iframe to fill 100% height, minus the header and footer**
    
Example:
    - `{"action": "height_change", "data": {"height": 654}}`
    - `{"action": "height_change", "data": {"height": "full_page"}}`