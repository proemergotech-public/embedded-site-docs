FORMAT: 1A

# Callback API

GENERATED, DO NOT EDIT, to regenerate:
- modify [definitions file](apimd/main.go)
- run `go run apimd/main.go`

## Group Http

### Callback api [/cams-api]

#### Escort status update [POST /cams-api/escorts/status]
Called when the escort (more specifically the channel he is broadcasting on) changes state (goes to free/private, etc.).

+ Request
    + Attributes
        + `channel_url`: `/channel/ozanna-102201` (string) - you can access the channel of the escort on this url
        + `preview_url`: `s/20795b9d03a8d3a821b0acb835062756/9bc1fb02-d57c-4be4-a982-969390000cd9/preview.jpg` (string) - stream server returns preview images in this path, only available if state is free. The host on which this path exists is: https://stream.escortcams.com/
        + `remote_id`: `hsfdg8z7784` (string) - unique identifier of the Member, can be either string or integer
        + `state`: `free` (string)

+ Response 200

+ Response 204
