FORMAT: 1A

# Dliver Escort Service

GENERATED, DO NOT EDIT, to regenerate:
- modify [definitions file](apimd/main.go)
- run `go run apimd/main.go`

## Group Http

### Escort api [/escort-api/v2]

#### Agency Create [POST /escort-api/v2/site-group/{site_group_code}/agencies/{profile_remote_id}]
Creates an agency user in our system and returns an auth_token for that agency. It also creates an escort user for each escort provided in the request.

+ Parameters
    + `profile_remote_id`: `78dzfiu43jf` (string) - unique identifier of the Agency, can be either string or integer
    + `site_group_code`: `your_site_group` (string)

+ Request
    + Attributes
        + `display_name`: `Agency Display Name` (string) - This text will be visible as the name of the Agency
        + `email`: `agency@email.com` (string)
        + `escorts`
            + `escort_remote_id`
                + `about`: `About me: ...` (string, optional)
                + `blocked_countries`(array)
                    + `ro` (string) - ISO 3166-1 Alpha-2
                    + `en` (string) - ISO 3166-1 Alpha-2
                + `channel_cover`
                    + `height`: `600` (number)
                    + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string) - channel_cover can be omitted, in which case we will find a landscape image from gallery_images
                    + `width`: `900` (number)
                + `display_name`: `Escort Display Name` (string) - This text will be visible as the name of the Escort
                + `email`: `escort@email.com` (string, optional)
                + `gallery_images`(array)
                    + (object)
                        + `height`: `900` (number)
                        + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string)
                        + `width`: `600` (number)
                    + (object)
                        + `height`: `900` (number)
                        + `url`: `https://your.cdn.domain/path/to/image_orig.jpeg` (string)
                        + `width`: `600` (number)
                + `languages`(array)
                    + `it` (string)
                    + `en` (string)
                + `legal_name`: `Escort Full Name` (string)
                + `nationality`: `it` (string, optional) - ISO 3166-1 Alpha-2
                + `profile_image`
                    + `height`: `600` (number)
                    + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string) - profile_image can be omitted, in which case we will find a landscape image from gallery_images
                    + `width`: `900` (number)
                + `subscription_active`: `true` (boolean, optional) - Whether the escort has a valid subscription.
        + `legal_name`: `Agency Full Name` (string)
        + `payout_data`
            + `paxum`
                + `date_of_birth`: `1983-12-11` (string)
                + `email`: `paxum@email.com` (string, optional)
                + `profile_name`: `Agency Full Name` (string)
            + `wire`
                + `aba_code`: `1111111111` (string, optional)
                + `account_holder_address`: `Account holder's address` (string, optional)
                + `account_holder_city`: `Account holder's city` (string, optional)
                + `account_holder_country`: `it` (string, optional) - ISO 3166-1 Alpha-2
                + `account_holder_name`: `Account holder's name` (string, optional)
                + `account_holder_zip`: `Account holder's zip code` (string, optional)
                + `account_number`: `4111-1111-1111-1111` (string, optional)
                + `bank_name`: `Bank's name` (string, optional)
                + `iban`: `IT60 X054 2811 1010 0000 0123 456` (string, optional)
                + `intermediary_bank`
                    + `bank_address`: `Intermediary bank's address` (string, optional)
                    + `bank_comment`: `Intermediary bank's comment` (string, optional)
                    + `bank_name`: `Intermediary bank's name` (string, optional)
                    + `swift_code`: `SWIFT22W` (string, optional)
                + `swift_code`: `SWIFT11S` (string, optional)
        + `signup_site_id`: `s_6356d829fa6bf47b74db9391c1998eb4` (string)

+ Response 200
    + Attributes
        + `auth_token`: `eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdXRoX3V1aWQiOiIzOTQ4MDViMS02OTBmLTRkZDYtODFkMC00MDVhZTdjZDU0NzAlmAJleHAiOjE1ODYxNjgwNDMsImp0aSI6ImFiYjRjNWFkNGFlYjRjZWRiNTA5NjQ4ZTk3MzVjMjY4YmViYmUyMjk0NTkzNDk0MGFmZGUwZTUzMzZlMWU3ZDEiLCJpYXQiOjE1ODU1NjMyNDN9.MKWUCR-J8Oj0ZRobIavVwEqKSSrTPOOXy4swnnbgVZhAYCS5xyOM1vZyhwIKE1XnY4AjAkVJXR_m9mxXxYyYZzZ` (string)

+ Response 400
    + Attributes
        + `error`
            + `code`: `ERR_VALIDATION` (string)
            + `details`(array)
                + (object)
                    + `field`: `site_group_code` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `profile_remote_id` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `email` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `display_name` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `signup_site_id` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `legal_name` (string)
                    + `validator`: `required` (string)

+ Response 401
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_MISSING` (string)

+ Response 403
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_INVALID` (string)

+ Response 409
    + Attributes
        + `error`
            + `code`: `ERR_AGENCY_ALREADY_EXISTS` (string)

+ Response 409
    + Attributes
        + `error`
            + `code`: `ERR_EMAIL_ALREADY_TAKEN` (string)

+ Response 500
    + Attributes
        + `error`
            + `code`: `ERR_INTERNAL` (string)

#### Agency Update [PATCH /escort-api/v2/site-group/{site_group_code}/agencies/{profile_remote_id}]
Update an existing agency. Data given here will update the existing one. All data are optional except the path parameters.

+ Parameters
    + `profile_remote_id`: `78dzfiu43jf` (string) - unique identifier of the Agency, can be either string or integer
    + `site_group_code`: `your_site_group` (string)

+ Request
    + Attributes
        + `display_name`: `Agency Display Name` (string) - This text will be visible as the name of the Agency
        + `email`: `agency@email.com` (string)
        + `escorts`
            + `escort_remote_id`
                + `about`: `About me: ...` (string, optional)
                + `blocked_countries`(array)
                    + `ro` (string) - ISO 3166-1 Alpha-2
                    + `en` (string) - ISO 3166-1 Alpha-2
                + `channel_cover`
                    + `height`: `600` (number)
                    + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string) - channel_cover can be omitted, in which case we will find a landscape image from gallery_images
                    + `width`: `900` (number)
                + `display_name`: `Escort Display Name` (string) - This text will be visible as the name of the Escort
                + `email`: `escort@email.com` (string, optional)
                + `gallery_images`(array)
                    + (object)
                        + `height`: `900` (number)
                        + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string)
                        + `width`: `600` (number)
                    + (object)
                        + `height`: `900` (number)
                        + `url`: `https://your.cdn.domain/path/to/image_orig.jpeg` (string)
                        + `width`: `600` (number)
                + `languages`(array)
                    + `it` (string)
                    + `en` (string)
                + `legal_name`: `Escort Full Name` (string)
                + `nationality`: `it` (string, optional) - ISO 3166-1 Alpha-2
                + `profile_image`
                    + `height`: `600` (number)
                    + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string) - profile_image can be omitted, in which case we will find a landscape image from gallery_images
                    + `width`: `900` (number)
                + `subscription_active`: `true` (boolean, optional) - Whether the escort has a valid subscription.
        + `legal_name`: `Agency Full Name` (string)
        + `payout_data`
            + `paxum`
                + `date_of_birth`: `1983-12-11` (string)
                + `email`: `paxum@email.com` (string, optional)
                + `profile_name`: `Agency Full Name` (string)
            + `wire`
                + `aba_code`: `1111111111` (string, optional)
                + `account_holder_address`: `Account holder's address` (string, optional)
                + `account_holder_city`: `Account holder's city` (string, optional)
                + `account_holder_country`: `it` (string, optional) - ISO 3166-1 Alpha-2
                + `account_holder_name`: `Account holder's name` (string, optional)
                + `account_holder_zip`: `Account holder's zip code` (string, optional)
                + `account_number`: `4111-1111-1111-1111` (string, optional)
                + `bank_name`: `Bank's name` (string, optional)
                + `iban`: `IT60 X054 2811 1010 0000 0123 456` (string, optional)
                + `intermediary_bank`
                    + `bank_address`: `Intermediary bank's address` (string, optional)
                    + `bank_comment`: `Intermediary bank's comment` (string, optional)
                    + `bank_name`: `Intermediary bank's name` (string, optional)
                    + `swift_code`: `SWIFT22W` (string, optional)
                + `swift_code`: `SWIFT11S` (string, optional)

+ Response 200

+ Response 400
    + Attributes
        + `error`
            + `code`: `ERR_VALIDATION` (string)
            + `details`(array)
                + (object)
                    + `field`: `site_group_code` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `profile_remote_id` (string)
                    + `validator`: `required` (string)

+ Response 401
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_MISSING` (string)

+ Response 403
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_INVALID|ERR_INVALID_AGENCY_STATUS` (string)

+ Response 404
    + Attributes
        + `error`
            + `code`: `ERR_AGENCY_NOT_FOUND` (string)

+ Response 500
    + Attributes
        + `error`
            + `code`: `ERR_INTERNAL` (string)

#### Agency Login [POST /escort-api/v2/site-group/{site_group_code}/agencies/{profile_remote_id}/login]
Logs in the agency user in our system and returns an auth_token for the agency. Additional data given here will update the existing one. All data are optional except the path parameters.

+ Parameters
    + `profile_remote_id`: `78dzfiu43jf` (string) - unique identifier of the Agency, can be either string or integer
    + `site_group_code`: `your_site_group` (string)

+ Request
    + Attributes
        + `display_name`: `Agency Display Name` (string) - This text will be visible as the name of the Agency
        + `email`: `agency@email.com` (string)
        + `escorts`
            + `escort_remote_id`
                + `about`: `About me: ...` (string, optional)
                + `blocked_countries`(array)
                    + `ro` (string) - ISO 3166-1 Alpha-2
                    + `en` (string) - ISO 3166-1 Alpha-2
                + `channel_cover`
                    + `height`: `600` (number)
                    + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string) - channel_cover can be omitted, in which case we will find a landscape image from gallery_images
                    + `width`: `900` (number)
                + `display_name`: `Escort Display Name` (string) - This text will be visible as the name of the Escort
                + `email`: `escort@email.com` (string, optional)
                + `gallery_images`(array)
                    + (object)
                        + `height`: `900` (number)
                        + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string)
                        + `width`: `600` (number)
                    + (object)
                        + `height`: `900` (number)
                        + `url`: `https://your.cdn.domain/path/to/image_orig.jpeg` (string)
                        + `width`: `600` (number)
                + `languages`(array)
                    + `it` (string)
                    + `en` (string)
                + `legal_name`: `Escort Full Name` (string)
                + `nationality`: `it` (string, optional) - ISO 3166-1 Alpha-2
                + `profile_image`
                    + `height`: `600` (number)
                    + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string) - profile_image can be omitted, in which case we will find a landscape image from gallery_images
                    + `width`: `900` (number)
                + `subscription_active`: `true` (boolean, optional) - Whether the escort has a valid subscription.
        + `legal_name`: `Agency Full Name` (string)
        + `payout_data`
            + `paxum`
                + `date_of_birth`: `1983-12-11` (string)
                + `email`: `paxum@email.com` (string, optional)
                + `profile_name`: `Agency Full Name` (string)
            + `wire`
                + `aba_code`: `1111111111` (string, optional)
                + `account_holder_address`: `Account holder's address` (string, optional)
                + `account_holder_city`: `Account holder's city` (string, optional)
                + `account_holder_country`: `it` (string, optional) - ISO 3166-1 Alpha-2
                + `account_holder_name`: `Account holder's name` (string, optional)
                + `account_holder_zip`: `Account holder's zip code` (string, optional)
                + `account_number`: `4111-1111-1111-1111` (string, optional)
                + `bank_name`: `Bank's name` (string, optional)
                + `iban`: `IT60 X054 2811 1010 0000 0123 456` (string, optional)
                + `intermediary_bank`
                    + `bank_address`: `Intermediary bank's address` (string, optional)
                    + `bank_comment`: `Intermediary bank's comment` (string, optional)
                    + `bank_name`: `Intermediary bank's name` (string, optional)
                    + `swift_code`: `SWIFT22W` (string, optional)
                + `swift_code`: `SWIFT11S` (string, optional)

+ Response 200
    + Attributes
        + `auth_token`: `eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdXRoX3V1aWQiOiIzOTQ4MDViMS02OTBmLTRkZDYtODFkMC00MDVhZTdjZDU0NzAlmAJleHAiOjE1ODYxNjgwNDMsImp0aSI6ImFiYjRjNWFkNGFlYjRjZWRiNTA5NjQ4ZTk3MzVjMjY4YmViYmUyMjk0NTkzNDk0MGFmZGUwZTUzMzZlMWU3ZDEiLCJpYXQiOjE1ODU1NjMyNDN9.MKWUCR-J8Oj0ZRobIavVwEqKSSrTPOOXy4swnnbgVZhAYCS5xyOM1vZyhwIKE1XnY4AjAkVJXR_m9mxXxYyYZzZ` (string)

+ Response 400
    + Attributes
        + `error`
            + `code`: `ERR_VALIDATION` (string)
            + `details`(array)
                + (object)
                    + `field`: `site_group_code` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `profile_remote_id` (string)
                    + `validator`: `required` (string)

+ Response 401
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_MISSING` (string)

+ Response 403
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_INVALID|ERR_INVALID_AGENCY_STATUS` (string)

+ Response 404
    + Attributes
        + `error`
            + `code`: `ERR_AGENCY_NOT_FOUND` (string)

+ Response 500
    + Attributes
        + `error`
            + `code`: `ERR_INTERNAL` (string)

#### Escort Create [POST /escort-api/v2/site-group/{site_group_code}/escorts/{profile_remote_id}]
Creates the escort user in our system and returns an auth_token for the escort.

+ Parameters
    + `profile_remote_id`: `78dzfiu43jf` (string) - unique identifier of the Escort, can be either string or integer
    + `site_group_code`: `your_site_group` (string)

+ Request
    + Attributes
        + `about`: `About me: ...` (string, optional)
        + `blocked_countries`(array)
            + `ro` (string) - ISO 3166-1 Alpha-2
            + `en` (string) - ISO 3166-1 Alpha-2
        + `channel_cover`
            + `height`: `600` (number)
            + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string) - channel_cover can be omitted, in which case we will find a landscape image from gallery_images
            + `width`: `900` (number)
        + `display_name`: `Escort Display Name` (string) - This text will be visible as the name of the Escort
        + `email`: `escort@email.com` (string)
        + `gallery_images`(array)
            + (object)
                + `height`: `900` (number)
                + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string)
                + `width`: `600` (number)
            + (object)
                + `height`: `900` (number)
                + `url`: `https://your.cdn.domain/path/to/image_orig.jpeg` (string)
                + `width`: `600` (number)
        + `languages`(array)
            + `it` (string)
            + `en` (string)
        + `legal_name`: `Escort Full Name` (string)
        + `nationality`: `it` (string, optional) - ISO 3166-1 Alpha-2
        + `payout_data`
            + `paxum`
                + `date_of_birth`: `1983-12-11` (string)
                + `email`: `paxum@email.com` (string, optional)
                + `profile_name`: `Escort Full Name` (string)
            + `wire`
                + `aba_code`: `1111111111` (string, optional)
                + `account_holder_address`: `Account holder's address` (string, optional)
                + `account_holder_city`: `Account holder's city` (string, optional)
                + `account_holder_country`: `it` (string, optional) - ISO 3166-1 Alpha-2
                + `account_holder_name`: `Account holder's name` (string, optional)
                + `account_holder_zip`: `Account holder's zip code` (string, optional)
                + `account_number`: `4111-1111-1111-1111` (string, optional)
                + `bank_name`: `Bank's name` (string, optional)
                + `iban`: `IT60 X054 2811 1010 0000 0123 456` (string, optional)
                + `intermediary_bank`
                    + `bank_address`: `Intermediary bank's address` (string, optional)
                    + `bank_comment`: `Intermediary bank's comment` (string, optional)
                    + `bank_name`: `Intermediary bank's name` (string, optional)
                    + `swift_code`: `SWIFT22W` (string, optional)
                + `swift_code`: `SWIFT11S` (string, optional)
        + `phone_number`: `+390507230064` (string)
        + `profile_image`
            + `height`: `600` (number)
            + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string) - profile_image can be omitted, in which case we will find a landscape image from gallery_images
            + `width`: `900` (number)
        + `signup_site_id`: `s_6356d829fa6bf47b74db9391c1998eb4` (string)
        + `subscription_active`: `true` (boolean, optional) - Whether the escort has a valid subscription.
        + `video_booking_contact_type`: `whatsapp` (string) - possible values whatsapp|sms|phone|viber
        + `video_booking_enabled`: `true` (boolean)
        + `video_booking_prices`(array)
            + (object)
                + `duration`: `15m` (string) - duration: 60s, 15m, 30m, 1h, etc...
                + `token`: `350` (number) - the amount of tokens this package worth

+ Response 200
    + Attributes
        + `auth_token`: `eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdXRoX3V1aWQiOiIzOTQ4MDViMS02OTBmLTRkZDYtODFkMC00MDVhZTdjZDU0NzAlmAJleHAiOjE1ODYxNjgwNDMsImp0aSI6ImFiYjRjNWFkNGFlYjRjZWRiNTA5NjQ4ZTk3MzVjMjY4YmViYmUyMjk0NTkzNDk0MGFmZGUwZTUzMzZlMWU3ZDEiLCJpYXQiOjE1ODU1NjMyNDN9.MKWUCR-J8Oj0ZRobIavVwEqKSSrTPOOXy4swnnbgVZhAYCS5xyOM1vZyhwIKE1XnY4AjAkVJXR_m9mxXxYyYZzZ` (string)

+ Response 400
    + Attributes
        + `error`
            + `code`: `ERR_VALIDATION` (string)
            + `details`(array)
                + (object)
                    + `field`: `site_group_code` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `profile_remote_id` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `email` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `display_name` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `signup_site_id` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `legal_name` (string)
                    + `validator`: `required` (string)

+ Response 401
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_MISSING` (string)

+ Response 403
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_INVALID` (string)

+ Response 409
    + Attributes
        + `error`
            + `code`: `ERR_ESCORT_ALREADY_EXISTS` (string)

+ Response 409
    + Attributes
        + `error`
            + `code`: `ERR_EMAIL_ALREADY_TAKEN` (string)

+ Response 500
    + Attributes
        + `error`
            + `code`: `ERR_INTERNAL` (string)

#### Escort Update [PATCH /escort-api/v2/site-group/{site_group_code}/escorts/{profile_remote_id}]
Update an existing escort. Data given here will update the existing one. All data are optional except the path parameters.

+ Parameters
    + `profile_remote_id`: `78dzfiu43jf` (string) - unique identifier of the Escort, can be either string or integer
    + `site_group_code`: `your_site_group` (string)

+ Request
    + Attributes
        + `about`: `About me: ...` (string, optional)
        + `blocked_countries`(array)
            + `ro` (string) - ISO 3166-1 Alpha-2
            + `en` (string) - ISO 3166-1 Alpha-2
        + `channel_cover`
            + `height`: `600` (number)
            + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string) - channel_cover can be omitted, in which case we will find a landscape image from gallery_images
            + `width`: `900` (number)
        + `display_name`: `Escort Display Name` (string, optional) - This text will be visible as the name of the Escort
        + `email`: `escort@email.com` (string, optional)
        + `gallery_images`(array)
            + (object)
                + `height`: `900` (number)
                + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string)
                + `width`: `600` (number)
            + (object)
                + `height`: `900` (number)
                + `url`: `https://your.cdn.domain/path/to/image_orig.jpeg` (string)
                + `width`: `600` (number)
        + `languages`(array)
            + `it` (string)
            + `en` (string)
        + `legal_name`: `Escort Full Name` (string, optional)
        + `nationality`: `it` (string, optional) - ISO 3166-1 Alpha-2
        + `payout_data`
            + `paxum`
                + `date_of_birth`: `1983-12-11` (string)
                + `email`: `paxum@email.com` (string, optional)
                + `profile_name`: `Escort Full Name` (string)
            + `wire`
                + `aba_code`: `1111111111` (string, optional)
                + `account_holder_address`: `Account holder's address` (string, optional)
                + `account_holder_city`: `Account holder's city` (string, optional)
                + `account_holder_country`: `it` (string, optional) - ISO 3166-1 Alpha-2
                + `account_holder_name`: `Account holder's name` (string, optional)
                + `account_holder_zip`: `Account holder's zip code` (string, optional)
                + `account_number`: `4111-1111-1111-1111` (string, optional)
                + `bank_name`: `Bank's name` (string, optional)
                + `iban`: `IT60 X054 2811 1010 0000 0123 456` (string, optional)
                + `intermediary_bank`
                    + `bank_address`: `Intermediary bank's address` (string, optional)
                    + `bank_comment`: `Intermediary bank's comment` (string, optional)
                    + `bank_name`: `Intermediary bank's name` (string, optional)
                    + `swift_code`: `SWIFT22W` (string, optional)
                + `swift_code`: `SWIFT11S` (string, optional)
        + `phone_number`: `+390507230064` (string)
        + `profile_image`
            + `height`: `600` (number)
            + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string) - profile_image can be omitted, in which case we will find a landscape image from gallery_images
            + `width`: `900` (number)
        + `subscription_active`: `true` (boolean, optional) - Whether the escort has a valid subscription.
        + `video_booking_contact_type`: `whatsapp` (string) - possible values whatsapp|sms|phone|viber
        + `video_booking_enabled`: `true` (boolean)
        + `video_booking_prices`(array)
            + (object)
                + `duration`: `15m` (string) - duration: 60s, 15m, 30m, 1h, etc...
                + `token`: `350` (number) - the amount of tokens this package worth

+ Response 200

+ Response 400
    + Attributes
        + `error`
            + `code`: `ERR_VALIDATION` (string)
            + `details`(array)
                + (object)
                    + `field`: `site_group_code` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `profile_remote_id` (string)
                    + `validator`: `required` (string)

+ Response 401
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_MISSING` (string)

+ Response 403
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_INVALID|ERR_INVALID_ESCORT_STATUS` (string)

+ Response 404
    + Attributes
        + `error`
            + `code`: `ERR_ESCORT_NOT_FOUND` (string)

+ Response 500
    + Attributes
        + `error`
            + `code`: `ERR_INTERNAL` (string)

#### Escort Login [POST /escort-api/v2/site-group/{site_group_code}/escorts/{profile_remote_id}/login]
Logs in the escort user in our system and returns an auth_token for the escort. Additional data given here will update the existing one. All data are optional except the path parameters.

+ Parameters
    + `profile_remote_id`: `78dzfiu43jf` (string) - unique identifier of the Escort, can be either string or integer
    + `site_group_code`: `your_site_group` (string)

+ Request
    + Attributes
        + `about`: `About me: ...` (string, optional)
        + `blocked_countries`(array)
            + `ro` (string) - ISO 3166-1 Alpha-2
            + `en` (string) - ISO 3166-1 Alpha-2
        + `channel_cover`
            + `height`: `600` (number)
            + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string) - channel_cover can be omitted, in which case we will find a landscape image from gallery_images
            + `width`: `900` (number)
        + `display_name`: `Escort Display Name` (string, optional) - This text will be visible as the name of the Escort
        + `email`: `escort@email.com` (string, optional)
        + `gallery_images`(array)
            + (object)
                + `height`: `900` (number)
                + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string)
                + `width`: `600` (number)
            + (object)
                + `height`: `900` (number)
                + `url`: `https://your.cdn.domain/path/to/image_orig.jpeg` (string)
                + `width`: `600` (number)
        + `languages`(array)
            + `it` (string)
            + `en` (string)
        + `legal_name`: `Escort Full Name` (string, optional)
        + `nationality`: `it` (string, optional) - ISO 3166-1 Alpha-2
        + `payout_data`
            + `paxum`
                + `date_of_birth`: `1983-12-11` (string)
                + `email`: `paxum@email.com` (string, optional)
                + `profile_name`: `Escort Full Name` (string)
            + `wire`
                + `aba_code`: `1111111111` (string, optional)
                + `account_holder_address`: `Account holder's address` (string, optional)
                + `account_holder_city`: `Account holder's city` (string, optional)
                + `account_holder_country`: `it` (string, optional) - ISO 3166-1 Alpha-2
                + `account_holder_name`: `Account holder's name` (string, optional)
                + `account_holder_zip`: `Account holder's zip code` (string, optional)
                + `account_number`: `4111-1111-1111-1111` (string, optional)
                + `bank_name`: `Bank's name` (string, optional)
                + `iban`: `IT60 X054 2811 1010 0000 0123 456` (string, optional)
                + `intermediary_bank`
                    + `bank_address`: `Intermediary bank's address` (string, optional)
                    + `bank_comment`: `Intermediary bank's comment` (string, optional)
                    + `bank_name`: `Intermediary bank's name` (string, optional)
                    + `swift_code`: `SWIFT22W` (string, optional)
                + `swift_code`: `SWIFT11S` (string, optional)
        + `phone_number`: `+390507230064` (string)
        + `profile_image`
            + `height`: `600` (number)
            + `url`: `https://your.cdn.domain/path/to/image_orig.jpg` (string) - profile_image can be omitted, in which case we will find a landscape image from gallery_images
            + `width`: `900` (number)
        + `subscription_active`: `true` (boolean, optional) - Whether the escort has a valid subscription.
        + `video_booking_contact_type`: `whatsapp` (string) - possible values whatsapp|sms|phone|viber
        + `video_booking_enabled`: `true` (boolean)
        + `video_booking_prices`(array)
            + (object)
                + `duration`: `15m` (string) - duration: 60s, 15m, 30m, 1h, etc...
                + `token`: `350` (number) - the amount of tokens this package worth

+ Response 200
    + Attributes
        + `auth_token`: `eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdXRoX3V1aWQiOiIzOTQ4MDViMS02OTBmLTRkZDYtODFkMC00MDVhZTdjZDU0NzAlmAJleHAiOjE1ODYxNjgwNDMsImp0aSI6ImFiYjRjNWFkNGFlYjRjZWRiNTA5NjQ4ZTk3MzVjMjY4YmViYmUyMjk0NTkzNDk0MGFmZGUwZTUzMzZlMWU3ZDEiLCJpYXQiOjE1ODU1NjMyNDN9.MKWUCR-J8Oj0ZRobIavVwEqKSSrTPOOXy4swnnbgVZhAYCS5xyOM1vZyhwIKE1XnY4AjAkVJXR_m9mxXxYyYZzZ` (string)

+ Response 400
    + Attributes
        + `error`
            + `code`: `ERR_VALIDATION` (string)
            + `details`(array)
                + (object)
                    + `field`: `site_group_code` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `profile_remote_id` (string)
                    + `validator`: `required` (string)

+ Response 401
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_MISSING` (string)

+ Response 403
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_INVALID|ERR_INVALID_ESCORT_STATUS` (string)

+ Response 404
    + Attributes
        + `error`
            + `code`: `ERR_ESCORT_NOT_FOUND` (string)

+ Response 500
    + Attributes
        + `error`
            + `code`: `ERR_INTERNAL` (string)

#### Member Create [POST /escort-api/v2/site-group/{site_group_code}/members/{profile_remote_id}]
Creates the member user in our system and returns an auth_token for the member.

+ Parameters
    + `profile_remote_id`: `78dzfiu43jf` (string) - unique identifier of the Member, can be either string or integer
    + `site_group_code`: `your_site_group` (string)

+ Request
    + Attributes
        + `display_name`: `Member Display Name` (string) - This text will be visible as the name of the Member
        + `email`: `member@email.com` (string)
        + `signup_site_id`: `s_6356d829fa6bf47b74db9391c1998eb4` (string)

+ Response 200
    + Attributes
        + `auth_token`: `eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdXRoX3V1aWQiOiIzOTQ4MDViMS02OTBmLTRkZDYtODFkMC00MDVhZTdjZDU0NzAlmAJleHAiOjE1ODYxNjgwNDMsImp0aSI6ImFiYjRjNWFkNGFlYjRjZWRiNTA5NjQ4ZTk3MzVjMjY4YmViYmUyMjk0NTkzNDk0MGFmZGUwZTUzMzZlMWU3ZDEiLCJpYXQiOjE1ODU1NjMyNDN9.MKWUCR-J8Oj0ZRobIavVwEqKSSrTPOOXy4swnnbgVZhAYCS5xyOM1vZyhwIKE1XnY4AjAkVJXR_m9mxXxYyYZzZ` (string)

+ Response 400
    + Attributes
        + `error`
            + `code`: `ERR_VALIDATION` (string)
            + `details`(array)
                + (object)
                    + `field`: `site_group_code` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `profile_remote_id` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `email` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `display_name` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `signup_site_id` (string)
                    + `validator`: `required` (string)

+ Response 401
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_MISSING` (string)

+ Response 403
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_INVALID` (string)

+ Response 409
    + Attributes
        + `error`
            + `code`: `ERR_MEMBER_ALREADY_EXISTS` (string)

+ Response 409
    + Attributes
        + `error`
            + `code`: `ERR_EMAIL_ALREADY_TAKEN` (string)
            
+ Response 500
    + Attributes
        + `error`
            + `code`: `ERR_INTERNAL` (string)

#### Member Login [POST /escort-api/v2/site-group/{site_group_code}/members/{profile_remote_id}/login]
Logs in the member user in our system and returns an auth_token for the member. Additional data given here will update the existing one

+ Parameters
    + `profile_remote_id`: `78dzfiu43jf` (string) - unique identifier of the Member, can be either string or integer
    + `site_group_code`: `your_site_group` (string)

+ Request
    + Attributes
        + `display_name`: `Member Display Name` (string, optional) - This text will be visible as the name of the Member
        + `email`: `member@email.com` (string, optional)

+ Response 200
    + Attributes
        + `auth_token`: `eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdXRoX3V1aWQiOiIzOTQ4MDViMS02OTBmLTRkZDYtODFkMC00MDVhZTdjZDU0NzAlmAJleHAiOjE1ODYxNjgwNDMsImp0aSI6ImFiYjRjNWFkNGFlYjRjZWRiNTA5NjQ4ZTk3MzVjMjY4YmViYmUyMjk0NTkzNDk0MGFmZGUwZTUzMzZlMWU3ZDEiLCJpYXQiOjE1ODU1NjMyNDN9.MKWUCR-J8Oj0ZRobIavVwEqKSSrTPOOXy4swnnbgVZhAYCS5xyOM1vZyhwIKE1XnY4AjAkVJXR_m9mxXxYyYZzZ` (string)

+ Response 400
    + Attributes
        + `error`
            + `code`: `ERR_VALIDATION` (string)
            + `details`(array)
                + (object)
                    + `field`: `site_group_code` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `profile_remote_id` (string)
                    + `validator`: `required` (string)

+ Response 401
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_MISSING` (string)

+ Response 403
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_INVALID|ERR_INVALID_MEMBER_STATUS` (string)

+ Response 404
    + Attributes
        + `error`
            + `code`: `ERR_MEMBER_NOT_FOUND` (string)

+ Response 500
    + Attributes
        + `error`
            + `code`: `ERR_INTERNAL` (string)

#### Member Purchase [POST /escort-api/v2/site-group/{site_group_code}/members/{profile_remote_id}/purchase]
Creates a transaction with the amount of tokens specified in the request body

+ Parameters
    + `profile_remote_id`: `hsfdg8z7784` (string) - unique identifier of the Member, can be either string or integer
    + `site_group_code`: `your_site_group` (string)

+ Request
    + Attributes
        + `purchase_amount`: `19.99` (number) - The amount spent in purchase_currency
        + `purchase_currency`: `USD` (string)
        + `purchase_remote_id`: `fhu84fklds` (string, optional) - unique identifier of the purchase transaction, needed for negate purchase, can be either string or integer
        + `site_id`: `s_6356d829fa6bf47b74db9391c1998eb4` (string)
        + `token_amount`: `100` (number)

+ Response 200

+ Response 400
    + Attributes
        + `error`
            + `code`: `ERR_VALIDATION` (string)
            + `details`(array)
                + (object)
                    + `field`: `site_group_code` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `profile_remote_id` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `site_id` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `token_amount` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `purchase_currency` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `purchase_amount` (string)
                    + `validator`: `required` (string)

+ Response 401
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_MISSING` (string)

+ Response 403
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_INVALID` (string)

+ Response 409
    + Attributes
        + `error`
            + `code`: `ERR_PURCHASE_ALREADY_EXISTS` (string)

+ Response 500
    + Attributes
        + `error`
            + `code`: `ERR_INTERNAL` (string)

#### Member Negate Purchase [POST /escort-api/v2/site-group/{site_group_code}/members/{profile_remote_id}/negate-purchase]
Negates an earlier transaction (refund)

+ Parameters
    + `profile_remote_id`: `hsfdg8z7784` (string) - unique identifier of the Member, can be either string or integer
    + `site_group_code`: `your_site_group` (string)

+ Request
    + Attributes
        + `purchase_remote_id`: `fhu84fklds` (string) - unique identifier of original purchase transaction, can be either string or integer
        + `site_id`: `s_6356d829fa6bf47b74db9391c1998eb4` (string)

+ Response 200

+ Response 400
    + Attributes
        + `error`
            + `code`: `ERR_VALIDATION` (string)
            + `details`(array)
                + (object)
                    + `field`: `site_group_code` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `profile_remote_id` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `site_id` (string)
                    + `validator`: `required` (string)
                + (object)
                    + `field`: `purchase_remote_id` (string)
                    + `validator`: `required` (string)

+ Response 401
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_MISSING` (string)

+ Response 403
    + Attributes
        + `error`
            + `code`: `ERR_SERVER_SECRET_INVALID` (string)

+ Response 404
    + Attributes
        + `error`
            + `code`: `ERR_PURCHASE_NOT_FOUND` (string)

+ Response 409
    + Attributes
        + `error`
            + `code`: `ERR_PURCHASE_ALREADY_NEGATED` (string)

+ Response 500
    + Attributes
        + `error`
            + `code`: `ERR_INTERNAL` (string)
