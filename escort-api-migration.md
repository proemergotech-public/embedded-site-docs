# Migrating from v1 to v2 api

In v1 api, you can call a single login, where you can supply all the data about the escort/member. We then decide which
fields to update on every requests, and which fields to store only when we first register the escort/member in our
system. 

In v2 api, there is a separate route for login and register (Create Member, Create Escort). Fields you send us during
login will be update for very login request, while fields you only want to set once can be sent on the register route.

## Recommended login/register logic

<table><tr><th>Old</th><th>New</th></tr>
<tr><td><pre>
call a single login route
</pre></td>
<td><pre>
- call a login route, with fields marked with UPDATE in the v1 docs
  - if it returns 404 error with ERR_MEMBER_NOT_FOUND/ERR_ESCORT_NOT_FOUND, call the create route
</pre></td></tr></table>

## Removed fields

- paxum_email field has been removed from escort login/create routes in favor of payout_data.paxum.email
- remote_id and profile_remote_id is now always in the url instead of body